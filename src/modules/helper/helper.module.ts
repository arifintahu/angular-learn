import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { OjekButtonComponent } from './components/button/button.component';
import { CommonModule } from '@angular/common';
import { OjekInputComponent } from './components/input/input.component';

@NgModule({
  imports : [
    CommonModule
  ],
  exports : [
    OjekButtonComponent,
    OjekInputComponent
  ],
  declarations: [
    OjekButtonComponent,
    OjekInputComponent
  ],
  providers: []
})
export class HelperModule { }
