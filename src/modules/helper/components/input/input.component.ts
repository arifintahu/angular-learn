import { Component,
    Input,
    Output,
    EventEmitter } from '@angular/core';

@Component({
    selector: 'ojek-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.css']
})
export class OjekInputComponent {
    @Input() inputIcon: string;
    @Input() password: boolean;
    @Input() inputPlaceholder: string;
    @Input() inputType: string;
    @Input() showPassword: string;
    changeIcon() {
        if (this.inputType === 'password') {
            this.showPassword = 'fas fa-eye';
            this.inputType = 'text';
        } else {
            this.showPassword = 'fas fa-eye-slash';
            this.inputType = 'password';
        }
    }
}
