import { OjekInputComponent } from './input.component';


describe('OjekButtonComponent', () => {
  it('should add the inputType text', () => {
    const input = new OjekInputComponent();
    input.inputType = 'text';

    expect(input.inputType).toEqual('text');
  });
  it('should showPassword', () => {
    const input = new OjekInputComponent();
    input.inputType = 'password';
    input.showPassword = 'fas fa-eye-slash';
    input.password = true;
    input.changeIcon();
    expect(input.inputType).toEqual('text');
  });
});
